package com.bor.blockchain.core.sqlparser;

import com.bor.blockchain.block.InstructionBase;


public interface InstructionParser {
    boolean parse(InstructionBase instructionBase);
}
