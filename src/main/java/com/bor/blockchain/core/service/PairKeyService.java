package com.bor.blockchain.core.service;

import com.bor.blockchain.block.PairKey;
import com.bor.blockchain.common.TrustSDK;
import com.bor.blockchain.common.exception.TrustSDKException;
import org.springframework.stereotype.Service;


@Service
public class PairKeyService {

    /**
     * 生成公私钥对
     * @return PairKey
     * @throws TrustSDKException TrustSDKException
     */
    public PairKey generate() throws TrustSDKException {
        return TrustSDK.generatePairKey(true);
    }
}
