package com.bor.blockchain.socket.pbft.listener;

import javax.annotation.Resource;

import com.bor.blockchain.socket.client.PacketSender;
import com.bor.blockchain.socket.packet.BlockPacket;
import com.bor.blockchain.socket.packet.PacketBuilder;
import com.bor.blockchain.socket.packet.PacketType;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.bor.blockchain.socket.body.VoteBody;
import com.bor.blockchain.socket.pbft.event.MsgPrepareEvent;
import com.bor.blockchain.socket.pbft.msg.VoteMsg;

/**
 */
@Component
public class PrepareEventListener {
    @Resource
    private PacketSender packetSender;

    /**
     * block已经开始进入Prepare状态
     *
     * @param msgPrepareEvent
     *         msgIsPrepareEvent
     */
    @EventListener
    public void msgIsPrepare(MsgPrepareEvent msgPrepareEvent) {
        VoteMsg voteMsg = (VoteMsg) msgPrepareEvent.getSource();

        //群发消息，通知别的节点，我已对该Block Prepare
        BlockPacket blockPacket = new PacketBuilder<>().setType(PacketType.PBFT_VOTE).setBody(new
                VoteBody(voteMsg)).build();

        //广播给所有人我已Prepare
        packetSender.sendGroup(blockPacket);
    }
}
