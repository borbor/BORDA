package com.bor.blockchain.socket.distruptor.base;


public interface MessageConsumer {
    void receive(BaseEvent baseEvent) throws Exception;
}
