package com.bor.blockchain.socket.distruptor;

import com.bor.blockchain.ApplicationContextProvider;
import com.bor.blockchain.socket.distruptor.base.BaseEvent;
import com.lmax.disruptor.EventHandler;

public class DisruptorClientHandler implements EventHandler<BaseEvent> {

    @Override
    public void onEvent(BaseEvent baseEvent, long sequence, boolean endOfBatch) throws Exception {
        ApplicationContextProvider.getBean(DisruptorClientConsumer.class).receive(baseEvent);
    }
}
